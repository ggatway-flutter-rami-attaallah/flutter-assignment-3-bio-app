import 'dart:ui';

import 'package:bioapp/screen/About_screen.dart';
import 'package:bioapp/screen/Home_screen.dart';
import 'package:bioapp/screen/Launch_screen.dart';
import 'package:flutter/material.dart';

void main(){
  runApp(BioApp());
}
class BioApp extends StatefulWidget {
  @override
  _BioAppState createState() => _BioAppState();
}

class _BioAppState extends State<BioApp> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      routes: {
        '/launch':(context)=>Launch(),
        '/about':(context)=>About(),
        '/home':(context)=>Home(),
      },
      initialRoute: '/launch',
        debugShowCheckedModeBanner: false,
    );
  }
}
