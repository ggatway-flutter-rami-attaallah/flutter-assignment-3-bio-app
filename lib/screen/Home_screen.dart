import 'dart:ui';

import 'package:bioapp/widget/text.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class Home extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          automaticallyImplyLeading: false,
          title: TextCustom('BIO App', 'Worksans', FontWeight.w400, 23,
              Colors.black, TextAlign.center),
          actions: <Widget>[
            PopupMenuButton(
                offset: Offset(-15, 39),
                onSelected: (int selected) {
                  if (selected == 1) {
                    Navigator.pushNamed(context, '/about');
                  }
                },
                itemBuilder: (context) {
                  return [
                    PopupMenuItem<int>(
                      value: 1,
                      child: TextCustom('About', 'Worksans', FontWeight.w700,
                          23, Colors.blue, TextAlign.center),
                    )
                  ];
                })
          ],
        ),
        body: Stack(children: [
          Image(
              height: double.infinity,
              fit: BoxFit.cover,
              image: AssetImage('images/img.jpg')),
          Padding(
            padding: const EdgeInsets.all(16.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [

                CircleAvatar(
                  backgroundImage: AssetImage('images/back.jpg'),
                  radius: 43,
                ),
                SizedBox(
                  height: 10,
                ),
                TextCustom('Rami Attaallah', 'Worksans', FontWeight.w700, 20,
                    Colors.black, TextAlign.center),
                SizedBox(
                  height: 10,
                ),
                TextCustom('GGatway-flutter course', 'Worksans', FontWeight.w400,
                    20, Colors.black, TextAlign.center),
                SizedBox(
                  height: 10,
                ),
                Divider(
                  color: Colors.black,
                  indent: 30,
                  endIndent: 30,
                  thickness: 1,
                ),
                SizedBox(
                  height: 10,
                ),
                Card(
                  shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(15),
                     side:BorderSide(
                       color: Colors.blue,
                       width: 2
                     ),
                ),


                  child: ListTile(
                    leading: Icon(Icons.email),
                    title: TextCustom(
                      'Email',
                      'Worksans',
                      FontWeight.w700,
                      20,
                      Colors.black,
                      TextAlign.left,
                    ),
                    subtitle: TextCustom('rameatalah@gmail.com', 'Worksans',
                        FontWeight.w400, 20, Colors.black, TextAlign.left),
                    trailing: Icon(Icons.send),
                  ),
                ),
                SizedBox(
                  height: 10,
                ),
                Card(
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(15),
                    side:BorderSide(
                        color: Colors.blue,
                        width: 2
                    ),
                  ),

                  child: ListTile(
                    leading: Icon(Icons.mobile_screen_share),
                    title: TextCustom(
                      'Mobile',
                      'Worksans',
                      FontWeight.w700,
                      20,
                      Colors.black,
                      TextAlign.left,
                    ),
                    subtitle: TextCustom('0599-1111-1111', 'Worksans',
                        FontWeight.w400, 20, Colors.black, TextAlign.left),
                    trailing: Icon(Icons.call),
                  ),
                ),
              ],
            ),
          ),

          Positioned(
            left: 0,
            right: 0,
            bottom: 10,
            child: TextCustom('Elancer-GGatway', 'Worksans', FontWeight.w400,
                15, Colors.white, TextAlign.center),
          )
        ]));
  }
}
