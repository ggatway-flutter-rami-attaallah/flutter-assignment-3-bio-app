import 'dart:ui';
import 'package:bioapp/widget/text.dart';
import 'package:flutter/material.dart';

class About extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          automaticallyImplyLeading: false,
          title: TextCustom(
              'BIO App', 'Worksans', FontWeight.w400, 23, Colors.white,TextAlign.center),
        ),
        body: Stack(children: [
          Image(
              height: double.infinity,
              fit: BoxFit.fill,
              image: AssetImage('images/ggatway.jpg')),
          Positioned(
              left: 0,
              right: 0,
              bottom: 10,
              child: TextCustom(
                  'Elancer', 'Worksans', FontWeight.w400, 20, Colors.black,TextAlign.center))
        ]));
  }
}
